﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : Vehicle
{
    public float Gas;
    public float GasMilage = 1;
    // Start is called before the first frame update
    void Start()
    {
        TurnOn();
    }

    // Update is called once per frame
    void Update()
    {

        MoveFoward();
    }
    public override void MoveFoward()
    {
        if (Gas > 0)
        {
            transform.position = transform.position + transform.forward * Speed;
        }
        else
            return;
        Gas -= GasMilage;
        print(Gas);
    }
}
