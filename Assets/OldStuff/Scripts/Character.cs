﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class Character : MonoBehaviour
{
    public CharacterData characterData;

    public string myName;
    public float healthVal;
    public float defence;
    public float damage;

    public TextMeshProUGUI CharacterHealth;

    // Start is called before the first frame update
    private void Awake()
    {
        InitCharacter(characterData);
    }
    public void InitCharacter(CharacterData data)
    {
        SetName(data.nameToSet);
        SetHealth(data.healthValToSet);
        SetDamage(data.DamageValToSet);
    }

    public void Attack()
    {
        AttackManager.Instance.ProcessAttack();
    }

    private void UpdateHealthText()
    {
        if(CharacterHealth != null)
            CharacterHealth.text = healthVal.ToString();
    }

    public void SetDamage(float d)
    {
        damage = d;
    }
    public void SetName(string name)
    {
        myName = name;
    }
    public void SetHealth(float health, float startDefence )
    {
        healthVal = health;
        defence = startDefence;
        UpdateHealthText();
    }
    
    public void SetHealth(float health)
    {
        healthVal = health;
        UpdateHealthText();
    }
    public float GetHealth()
    {
        return healthVal;
    }  
    public float GetDamage()
    {
        return damage;
    }   
    public float GetDefence()
    {
        return defence;
    }    
    public string GetName()
    {
        return myName;
    }
    public void TakeDamage(float damage)
    {
        if (damage < 0)
        {
            damage = 0;
        }
        healthVal -= damage;
        UpdateHealthText();
        if (healthVal <= 0)
        {
            Destroy(gameObject);
        }
    }
}
