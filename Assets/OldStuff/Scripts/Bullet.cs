﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float Speed;
    public float BulletDrop;
    public float BulletSize;
    
    public float Lifetime;


    public void Start()
    {
        int total = 0;

        for (int i = 0; i <= 10; i++)//11
        {

            for (int j = 0; j <= 5; j++)//total 
            {

                total += j;

            }

        }

        print(total);
    }
    // Update is called once per frame
    void Update()
    {
        //moveent
        transform.position = transform.position + transform.forward * (Speed * Time.deltaTime);
        //Timer
        Lifetime -= Time.deltaTime;
        if (Lifetime <= 0)
        {
            Destroy(transform.gameObject);
        }
    }
}
