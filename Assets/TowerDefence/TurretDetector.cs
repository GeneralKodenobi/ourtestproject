﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretDetector : MonoBehaviour
{

    private Turret myTurret;
    public List<GameObject> ChildrenList = new List<GameObject>();
    public GameObject ObjecToGetChildrenFRom;
    
    // Start is called before the first frame update
    void Start()
    {
        myTurret = GetComponentInParent<Turret>();

        for (int i = 0; i < ObjecToGetChildrenFRom.transform.childCount; i++)
        {
            ChildrenList.Add(ObjecToGetChildrenFRom.transform.GetChild(i).gameObject);
        }
    
    }


    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Enemy")
        {
            if (!myTurret.HasTarget())
            {
                myTurret.SetTarget(other.gameObject);
            }
            else//if we have target
            {
                myTurret.AddPossibleTarget(other.gameObject);
            }
        }

    }
    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "Enemy")
        {
            if (myTurret.HasTarget())
            {
                myTurret.ClearTarget(other.gameObject);
            }
            else
            {
                myTurret.RemovePossibleTarget(other.gameObject);
            }
        }

    }


}
