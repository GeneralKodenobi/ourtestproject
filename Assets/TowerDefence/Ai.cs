﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ai : MonoBehaviour
{
    public float HealthValue;
    public Transform target;
    private NavMeshAgent agent;
    public GameObject ExplosionEffect;
    //public TowerDefenceManager Manager;

    // Start is called before the first frame update
    void Awake()
    {
        agent =GetComponent<NavMeshAgent>();
    }

    public void SetTarget(Transform target)
    {
        agent.SetDestination(target.position);

    }
    // Update is called once per frame
    void Update()
    {
        if (agent.remainingDistance <= 1)
        {
            agent.isStopped = true;
            TowerDefenceManager.Instance.EnemyArrived();

          //  Manager.EnemyArrived();
        }
    }

    public bool didHitKill( float dmgVal)
    {
        HealthValue -= dmgVal;
        if (HealthValue < 0)
        {
            // Manager.OnAIKilled();
            TowerDefenceManager.Instance.OnAIKilled();
            ExplosionEffect.SetActive(true);
            ExplosionEffect.transform.parent = null;
            Destroy(gameObject);
            return true;
        }

        return false;
    }
}
