﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffAfterDelay : MonoBehaviour
{
    public float delayTime;// in seconds

    // Start is called before the first frameupdate 
    void Start()
    {
        StartCoroutine(HideDelay(delayTime));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    int GetValue()
    {
        return 0;
    }

    IEnumerator HideDelay(float timeTODelay)// known as a courountine 
    {
        //Before delay
        print("Before The delay");
        //THE delay 
        yield return new WaitForSeconds(timeTODelay);
        print("After the Delay 11");
        yield return new WaitForSeconds(timeTODelay);
        print("After the Delay 2");

        //after Delay
        gameObject.SetActive(false);
    }

}
