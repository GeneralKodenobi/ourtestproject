﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour
{

    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PlayerInput();
    }


    void PlayerInput()
    {
        float playerFwd = Input.GetAxis("Vertical");
        float playeLR= Input.GetAxis("Horizontal");

        MoveFwd(playerFwd*Time.deltaTime*speed);
        MoveLeftRight(playeLR * Time.deltaTime*speed);

    }

    void MoveFwd(float speed)
    {
        transform.position = transform.position + (transform.forward * speed);
    }
    void MoveLeftRight(float speed)
    {
        transform.position = transform.position + (transform.right * speed);

    }
    private void OnTriggerEnter(Collider other)
    {
        //check if our spheres 
        // need to have a refeneence to cube manager
        // tell cube manager that we have picked up an objt
    }
}
